# typst dockerfile

Ce projet reprend le [dockerfile de Typst](https://github.com/typst/typst/blob/main/Dockerfile), et construit une image Docker qu'il stocke dans le registre interne de cette instance.